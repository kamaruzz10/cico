import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class ToastService {

  constructor(
    public toastController: ToastController
  ) { }

  async presentToast(msg,status) {
    const toast = await this.toastController.create({
      message: msg,
      color: (status === 1)? 'success' : 'danger',
      duration: 2000
    });
    toast.present();
  }
}

import { Component, OnInit } from '@angular/core';
import { IonRouterOutlet, ModalController, NavController } from '@ionic/angular';
import { ModalsubscriptionComponent } from 'src/app/components/modalsubscription/modalsubscription.component';

@Component({
  selector: 'app-subscription',
  templateUrl: './subscription.page.html',
  styleUrls: ['./subscription.page.scss'],
})
export class SubscriptionPage implements OnInit {

  constructor(
    public nav: NavController,
    private modalCtrl: ModalController, 
    private routerOutlet: IonRouterOutlet
  ) { }

  ngOnInit() {
  }

  async _openModalSubscription(){
    //console.log('open modal')
    const modal = await this.modalCtrl.create({
      component: ModalsubscriptionComponent,
      //cssClass:'my-modalsubscription-component-css',
      cssClass:'modalsubscription-css',
      swipeToClose: true,
      //presentingElement: this.routerOutlet.nativeEl

      presentingElement: await this.modalCtrl.getTop()
    })

    modal.onDidDismiss()//.then((data:any)=>{
      //console.log(data)
      //if (data.data)
      //this.fromModal=data.data.fromModal;
    //})

    return await modal.present();

  }
}

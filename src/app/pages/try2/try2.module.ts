import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Try2PageRoutingModule } from './try2-routing.module';

import { Try2Page } from './try2.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Try2PageRoutingModule
  ],
  declarations: [Try2Page]
})
export class Try2PageModule {}

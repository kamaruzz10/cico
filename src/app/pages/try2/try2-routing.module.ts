import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Try2Page } from './try2.page';

const routes: Routes = [
  {
    path: '',
    component: Try2Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Try2PageRoutingModule {}

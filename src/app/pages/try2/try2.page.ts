import { Component, OnInit } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { LocationAccuracy } from '@ionic-native/location-accuracy/ngx';
import { EnvService } from 'src/app/services/env.service';
import { HttpClient } from '@angular/common/http';

declare var google;


@Component({
  selector: 'app-try2',
  templateUrl: './try2.page.html',
  styleUrls: ['./try2.page.scss'],
})
export class Try2Page {

  locationCoords: any;
  timetest: any;
  map: any;
  

  constructor(
    private geolocation: Geolocation,
    private locationAccuracy: LocationAccuracy,
    public env: EnvService,
    public http: HttpClient
  ) {
    this.locationCoords = {
      latitude: "",
      longitude: "",
      location: "",
      accuracy: "",
      address: "",
      timestamp: ""
    }
    this.timetest = Date.now();
  }


  requestGPSPermission() {
    this.locationAccuracy.canRequest().then((canRequest: boolean) => {
      if (canRequest) {
        console.log('Request successful'),
            error => {
              //Show alert if user click on 'No Thanks'
              alert('requestPermission Error requesting location permissions ' + error)
            }
      }
    });
    this. getLocationCoordinates();
  }

  askToTurnOnGPS() {
    // this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
    //   () => {
    //     // When GPS Turned ON call method to get Accurate location coordinates
    //     this.getLocationCoordinates()
    //   },
    //   error => alert('Error requesting location permissions.Please enable location setting for apps to working properly')
    // );

  }

  // Methos to get device accurate coordinates using device GPS
  getLocationCoordinates() {
    this.geolocation.getCurrentPosition().then((resp) => {
      this.locationCoords.latitude = resp.coords.latitude;
      this.locationCoords.longitude = resp.coords.longitude;

      this.locationCoords.location=this.locationCoords.latitude + ',' + this.locationCoords.longitude;

      let jsonAddress = 'https://reverse.geocoder.ls.hereapi.com/6.2/reversegeocode.json?prox=' + this.locationCoords.location + '&mode=retrieveAddresses&maxresults=1&gen=9&apiKey=' + this.env.hereApiKey;

      new Promise((resolve, reject) => {
        this.http.get(jsonAddress)
          .subscribe(response => {
            this.locationCoords.address = response['Response']['View'][0]['Result'][0]['Location']['Address']['Label'];
          }, error => { reject(error) });
      });

      this.locationCoords.accuracy = resp.coords.accuracy;
      this.locationCoords.timestamp = resp.timestamp;

      this.map = 'https://image.maps.ls.hereapi.com/mia/1.6/mapview?apiKey=' + this.env.hereApiKey + '&c=' + this.locationCoords.location + '&ctr=' + this.locationCoords.location + '&z=18&h=400&w=600&style=mini&ml=eng&t=2&q=30';
      
    }).catch((error) => {
      alert('Error getting location' + error);
    });
  }

}
 
 

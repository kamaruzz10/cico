import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { WorkingdaysPage } from './workingdays.page';

const routes: Routes = [
  {
    path: '',
    component: WorkingdaysPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class WorkingdaysPageRoutingModule {}

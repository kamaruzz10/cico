import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { WorkingdaysPageRoutingModule } from './workingdays-routing.module';

import { WorkingdaysPage } from './workingdays.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    WorkingdaysPageRoutingModule
  ],
  declarations: [WorkingdaysPage]
})
export class WorkingdaysPageModule {}

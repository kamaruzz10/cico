import { Component, OnInit } from '@angular/core';
import { HTTP } from '@ionic-native/http/ngx';
import { LoadingController, NavController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { EnvService } from 'src/app/services/env.service';
import { ToastService } from 'src/app/services/toast.service';

@Component({
  selector: 'app-workingdays',
  templateUrl: './workingdays.page.html',
  styleUrls: ['./workingdays.page.scss'],
})
export class WorkingdaysPage implements OnInit {

  form: any

  constructor(
    public loading: LoadingController,
    public http: HTTP,
    public storage: Storage,
    public toast:ToastService,
    public nav: NavController,
    public env: EnvService) { 
      this.form = { company_name: null, registration_no: null, firstname: null, lastname: null, 
        phone_no: null, email: null, username: null, password: null }
    }

  ngOnInit() {
  }

  async workingdays() {

    const loader = await this.loading.create();
    loader.present();

    return new Promise((resolve, reject) => {
      this.http.post(this.env.API_URL + 'api/working-days', this.form, this.env.header
      ).then(response => {

        let record = JSON.parse(response.data)

        if (record.status == 1) {
          loader.dismiss()
          this.toast.presentToast(record.message, 1);
        } else {
          loader.dismiss()
          this.toast.presentToast('Update Failed.', 2);
        }

      }), error => {
        this.toast.presentToast('Failed', 2);
        loader.dismiss()
      };
    })
  }

}

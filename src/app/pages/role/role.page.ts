import { Component, OnInit } from '@angular/core';
import { ModalController, NavController } from '@ionic/angular';
import { ModalroleComponent } from 'src/app/components/modalrole/modalrole.component';
import { HTTP } from '@ionic-native/http/ngx';
import { EnvService } from 'src/app/services/env.service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-role',
  templateUrl: './role.page.html',
  styleUrls: ['./role.page.scss'],
})
export class RolePage implements OnInit {

  session: any;
  header: any;
  rolelist: any;

  constructor(
    public nav: NavController,
    private modalCtrl: ModalController,
    public http: HTTP,
    public env: EnvService,
    public storage: Storage
  ) {
    this.storage.get('session').then(val => {
      this.session = val
      this.header = {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json',
        'Athorization': 'Bearer ' + this.session.access_token
      }
    })
   }

  ngOnInit() {
  }

  ionViewDidEnter() {
    this.http.get(this.env.API_URL + 'api/user-role', "",this.header)
    .then(response => {
      let data = JSON.parse(response.data)
      this.rolelist = data
    })
  }


  async _openModalRole(){
    //console.log('open modal')
    const modal = await this.modalCtrl.create({
      component: ModalroleComponent,
      cssClass:'modalrole-css',
      swipeToClose: true,
      //presentingElement: this.routerOutlet.nativeE1

      presentingElement: await this.modalCtrl.getTop()
    })

    modal.onDidDismiss();
    return await modal.present();
  }

}

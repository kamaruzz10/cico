import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { ModalController } from '@ionic/angular';
import { ModalComponent } from 'src/app/components/modal/modal.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  constructor(
    public storage: Storage,
    public nav: NavController,
    private modalCtrl: ModalController
  ) { }

  ngOnInit() {
  }

  logout() {
    this.storage.clear();
    this.nav.navigateRoot('/login');
  }

  async _openModal(){
    //console.log('open modal')
    const modal = await this.modalCtrl.create({
      component: ModalComponent,
      cssClass:'my-modal-component-css',
      swipeToClose: true,
      //presentingElement: this.routerOutlet.nativeEl

      presentingElement: await this.modalCtrl.getTop()
    })

    modal.onDidDismiss()//.then((data:any)=>{
      //console.log(data)
      //if (data.data)
      //this.fromModal=data.data.fromModal;
    //})

    return await modal.present();

  }

}

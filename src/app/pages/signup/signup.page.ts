import { Component, OnInit } from '@angular/core';
import { HTTP } from '@ionic-native/http/ngx';
import { LoadingController, NavController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { EnvService } from 'src/app/services/env.service';
import { ToastService } from 'src/app/services/toast.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {

  showPassword = false;
  passwordToggleIcon = 'eye';
  form: any
  constructor(
    public loading: LoadingController,
    public http: HTTP,
    public storage: Storage,
    public toast:ToastService,
    public nav: NavController,
    public env: EnvService
  ) {
    this.form = { company_name: null, registration_no: null, firstname: null, 
      lastname: null, phone_no: null, email: null, username: null, password: null }
  }

  ngOnInit() {
  }

  togglePassword(): void {
    this.showPassword = !this.showPassword;

    // eslint-disable-next-line eqeqeq
    if (this.passwordToggleIcon == 'eye') {
      this.passwordToggleIcon = 'eye-off';
    }
    else {
      this.passwordToggleIcon = 'eye';
    }

  }

  async signup() {

    const loader = await this.loading.create();
    loader.present();

    return new Promise((resolve, reject) => {
      this.http.post(this.env.API_URL + 'api/signup', this.form, this.env.header
      ).then(response => {

        let record = JSON.parse(response.data)

        if (record.status == 1) {
          loader.dismiss()
          this.nav.navigateBack('/login')
          this.toast.presentToast(record.message, 1);
        } else {
          loader.dismiss()
          this.toast.presentToast('Sign Up Failed.', 2);
        }

      }), error => {
        this.toast.presentToast('Failed', 2);
        loader.dismiss()
      };
    })
  }


}

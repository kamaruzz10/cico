import { Component, OnInit } from '@angular/core';
import { HTTP } from '@ionic-native/http/ngx';
//import { LoadingController} from "@ionic/angular";
import { PickerController } from '@ionic/angular';
import { PickerOptions } from '@ionic/core';
import { EnvService } from 'src/app/services/env.service';
import { Storage } from '@ionic/storage';
//import { ToastService } from 'src/app/services/toast.service';

@Component({
  selector: 'app-myaccount',
  templateUrl: './myaccount.page.html',
  styleUrls: ['./myaccount.page.scss'],
})
export class MyaccountPage implements OnInit {

  gender = '';
  maritalstatus = '';
  session: any;
  header: any;
  profiledata: any;
  // form: any;

  constructor(
    private pickerCtrl: PickerController,
    public http: HTTP,
    public env: EnvService,
    public storage: Storage,
    // public loading: LoadingController,
    // public toast: ToastService
    ) { 
      this.storage.get('session').then(val => {
        this.session = val
        this.header = {
          'Access-Control-Allow-Origin': '*',
          'Content-Type': 'application/json',
          'Athorization': 'Bearer ' + this.session.access_token
        }
      })

      // this.form = { username: null, phone_no: null, firstname: null, lastname: null,
      //   email: null, password: null, new_password: null, re_password: null }
    }
  

  ngOnInit() {
  }

  // select state
  async showMaritalStatus(){
    let opts: PickerOptions = {
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel'
        },
        {
          text:'Done'
        }
      ],

      columns: [
        {
          name: 'maritalstatus',
          options: [
            { text: 'Single', value: 'single'},
            { text: 'Married', value: 'married'},
            
          ]
        }
      ]
    };
    let picker = await this.pickerCtrl.create(opts);
    picker.present();
    picker.onDidDismiss().then(async data => {
      let col = await picker.getColumn('maritalstatus');
      console.log('col: ', col);
      this.maritalstatus = col.options[col.selectedIndex].text;
    });
  }


  // select gender
  async showGender(){
    let opts: PickerOptions = {
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel'
        },
        {
          text: 'Done'
        }
      ],
      columns: [
        {
          name: 'gender',
          options: [
            { text: 'Male', value: 'male'},
            { text: 'Female', value: 'female'}
          ]
        }
      ]
    };
    let picker = await this.pickerCtrl.create(opts);
    picker.present();
    picker.onDidDismiss().then(async data => {
      let col = await picker.getColumn('gender');
      console.log('col: ', col);
      this.gender = col.options[col.selectedIndex].text;
    });
  }

  ionViewDidEnter(){
    this.http.get(this.env.API_URL + 'api/profile-data', "", this.header)
    .then(response => {
      let data = JSON.parse(response.data)
      this.profiledata = data
    })

  }

  // async updateprofile() {

  //   const loader = await this.loading.create();
  //   loader.present();

  //   return new Promise((resolve, reject) => {
  //     this.http.post(this.env.API_URL + 'api/update-profile', this.form, this.header
  //     ).then(response => {

  //       let record = JSON.parse(response.data)

  //       if (record.status == 1) {
  //         loader.dismiss()
  //         this.toast.presentToast(record.message, 1);
  //       } else {
  //         loader.dismiss()
  //         this.toast.presentToast('Update Failed.', 2);
  //       }

  //     }), error => {
  //       this.toast.presentToast('Failed', 2);
  //       loader.dismiss()
  //     };
  //   })
  // }
}
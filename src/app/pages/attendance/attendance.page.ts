import { Component, OnInit } from '@angular/core';
import { HTTP } from '@ionic-native/http/ngx';
import { IonRouterOutlet, ModalController, NavController } from '@ionic/angular';
import { ModaldetailsComponent } from 'src/app/components/modaldetails/modaldetails.component';
import { EnvService } from 'src/app/services/env.service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-attendance',
  templateUrl: './attendance.page.html',
  styleUrls: ['./attendance.page.scss'],
})
export class AttendancePage implements OnInit {

  employeelist: any;
  userattendance: any;
  session: any;
  header: any;

  constructor(
    public nav: NavController,
    private modalCtrl: ModalController,
    public http: HTTP,
    public storage: Storage,
    public env: EnvService
  ) {
    this.storage.get('session').then(val => {
      this.session = val
      this.header = {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json',
        'Athorization': 'Bearer ' + this.session.access_token
      }
    })
   }

  ngOnInit() {
  }

  // ionViewDidEnter() {
  //   this.http.get(this.env.API_URL + 'api/employee-list', "",this.header)
  //   .then(response => {
  //     let data = JSON.parse(response.data)
  //     this.employeelist = data
  //   })

  //   this.http.get(this.env.API_URL + 'api/user-attendance', "",this.header)
  //   .then(response => {
  //     let data = JSON.parse(response.data)
  //     this.userattendance = data
  //   })
  // }

  async _openModalDetails(){
    const modal = await this.modalCtrl.create({
      component: ModaldetailsComponent,
      cssClass:'modaldetails-css',
      swipeToClose: true,
      //presentingElement: this.routerOutlet.nativeE1

      presentingElement: await this.modalCtrl.getTop()
    })

    modal.onDidDismiss()

    return await modal.present();
  }

  filter(){
    
  }

}

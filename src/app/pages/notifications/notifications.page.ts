import { Component, OnInit } from '@angular/core';
import { HTTP } from '@ionic-native/http/ngx';
import { EnvService } from 'src/app/services/env.service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.page.html',
  styleUrls: ['./notifications.page.scss'],
})
export class NotificationsPage implements OnInit {

  //@ViewChild('slides', { static: true }) slider: IonSlides;  
  //segment = 0; 
  segmentModel = "notifications";
  session: any;
  header: any;
  notificationlist: any;

  constructor(
    public http: HTTP,
    public env: EnvService,
    public storage: Storage
  ) {
    this.storage.get('session').then(val => {
      this.session = val
      this.header = {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json',
        'Athorization': 'Bearer ' + this.session.access_token
      }
    })
  }

  ngOnInit() {

  }

  ionViewDidEnter() {
    this.http.get(this.env.API_URL + 'api/notifications', "",this.header)
    .then(response => {
      let data = JSON.parse(response.data)
      this.notificationlist = data
    })
  }

  
}

/* eslint-disable @typescript-eslint/no-unused-expressions */
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HTTP } from '@ionic-native/http/ngx';
import { LoadingController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { EnvService } from 'src/app/services/env.service';
import { ToastService } from 'src/app/services/toast.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  showPassword = false;
  passwordToggleIcon = 'eye';

  form: any
  constructor(
    // private appVersion: AppVersion,
    private router: Router,
    public env: EnvService,
    public http: HTTP,
    public toast:ToastService,
    public loading: LoadingController,
    public storage: Storage
  ) {
    this.form = { username: null, password: null }
  }

  ngOnInit() {
  }

  togglePassword(): void {
    this.showPassword = !this.showPassword;

    // eslint-disable-next-line eqeqeq
    if (this.passwordToggleIcon == 'eye') {
      this.passwordToggleIcon = 'eye-off';
    }
    else {
      this.passwordToggleIcon = 'eye';
    }

  }

  async login() {

    const loader = await this.loading.create();
    loader.present();

    return new Promise((resolve, reject) => {
      this.http.post(this.env.API_URL + 'api/login', this.form, this.env.header
      ).then(response => {

        let record = JSON.parse(response.data)

        if (record.status == 1) {
          loader.dismiss()
          this.storage.set('session', record.data)
          this.router.navigate(['tabs/home']);
          this.toast.presentToast(record.message, 1);
        } else {
          loader.dismiss()
          this.toast.presentToast('Incorrect username or password.', 2);
        }

      }), error => {
        this.toast.presentToast('Failed', 2);
        loader.dismiss()
      };
    })
  }
}

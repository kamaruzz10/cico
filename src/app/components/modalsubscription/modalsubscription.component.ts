import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-modalsubscription',
  templateUrl: './modalsubscription.component.html',
  styleUrls: ['./modalsubscription.component.scss'],
})
export class ModalsubscriptionComponent implements OnInit {

  @Input() name: string;
  @Input() type: string;

  constructor(private modalCtrl:ModalController) { }

  ngOnInit() {}

  _dismiss(){
    console.log("dismiss");
    this.modalCtrl.dismiss();
    //this.modalCtrl.dismiss({
      //"fromModal": "Subscribed Channel"
    //})
  }

}

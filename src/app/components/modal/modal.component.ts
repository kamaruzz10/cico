import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { LoadingController, NavController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { EnvService } from 'src/app/services/env.service';
import { ToastService } from 'src/app/services/toast.service';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { HttpClient } from '@angular/common/http';
import { LocationAccuracy } from '@ionic-native/location-accuracy/ngx';
import { HTTP } from '@ionic-native/http/ngx';

declare var H: any;

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
  
})
export class ModalComponent implements OnInit {

  locationCoords: any;
  map: any;
  dateTime: Date;
  // form: any;

  constructor(
    private modalCtrl: ModalController,
    public loadingCtrl: LoadingController,
    public http: HttpClient,
    public storage: Storage,
    public toast:ToastService,
    public navCtrl: NavController,
    public env: EnvService,
    private geolocation: Geolocation,
    private locationAccuracy:LocationAccuracy,
    public https: HTTP
    )
  {
    // this.form = {date: null, location: null, notes: null}

    this.locationCoords = {
      latitude: "",
      longitude: "",
      location: "",
      accuracy: "",
      address: ""
    }
  }

  ngOnInit() {

    this.dateTime = new Date();
    this. getLocationCoordinates();
    
  }

  requestGPSPermission() {
    this.locationAccuracy.canRequest().then((canRequest: boolean) => {
      if (canRequest) {
        console.log('Request successful'),
            error => {
              //Show alert if user click on 'No Thanks'
              alert('requestPermission Error requesting location permissions ' + error)
            }
      }
    });
    
  }

  askToTurnOnGPS() {
    // this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
    //   () => {
    //     // When GPS Turned ON call method to get Accurate location coordinates
    //     this.getLocationCoordinates()
    //   },
    //   error => alert('Error requesting location permissions.Please enable location setting for apps to working properly')
    // );

  }

  // Methos to get device accurate coordinates using device GPS
  getLocationCoordinates() {
    this.geolocation.getCurrentPosition().then((resp) => {
      this.locationCoords.latitude = resp.coords.latitude;
      this.locationCoords.longitude = resp.coords.longitude;

      this.locationCoords.location=this.locationCoords.latitude + ',' + this.locationCoords.longitude;

      let jsonAddress = 'https://reverse.geocoder.ls.hereapi.com/6.2/reversegeocode.json?prox=' + this.locationCoords.location + '&mode=retrieveAddresses&maxresults=1&gen=9&apiKey=' + this.env.hereApiKey;

      new Promise((resolve, reject) => {
        this.http.get(jsonAddress)
          .subscribe(response => {
            this.locationCoords.address = response['Response']['View'][0]['Result'][0]['Location']['Address']['Label'];
          }, error => { reject(error) });
      });

      // this.http.get(jsonAddress, '', '')
      //     .then(response => {
      //       let record = JSON.parse(response.data)
      //       this.address = record.Response.View[0].Result[0].Location.Address.Label;
      //     }, error => { reject(error) });
      // });

      this.locationCoords.accuracy = resp.coords.accuracy;

      this.map = 'https://image.maps.ls.hereapi.com/mia/1.6/mapview?apiKey=' + this.env.hereApiKey + '&c=' + this.locationCoords.location + '&ctr=' + this.locationCoords.location + '&z=18&h=400&w=600&style=mini&ml=eng&t=2&q=30';
      
    }).catch((error) => {
      alert('Error getting location' + error);
    });
  }


  // close modal component
  _dismiss(){
    console.log("dismiss");
    this.modalCtrl.dismiss();
  }


  // async clockin() {

  //   const loader = await this.loadingCtrl.create();
  //   loader.present();

  //   return new Promise((resolve, reject) => {
  //     this.https.post(this.env.API_URL + 'api/clock-in', this.form, this.env.header
  //     ).then(response => {

  //       let record = JSON.parse(response.data)

  //       if (record.status == 1) {
  //         loader.dismiss()
  //         this.toast.presentToast(record.message, 1);
          
  //       } else {
  //         loader.dismiss()
  //         this.toast.presentToast('Clockin Failed.', 2);
  //       }

  //     }), error => {
  //       this.toast.presentToast('Failed', 2);
  //       loader.dismiss()
  //     };
  //   })
  // }
    
}

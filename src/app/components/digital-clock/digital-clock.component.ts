import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';

@Component({
  selector: 'app-digital-clock',
  templateUrl: './digital-clock.component.html',
  styleUrls: ['./digital-clock.component.scss'],
})
export class DigitalClockComponent implements OnInit {

  public day: string;
  time: any

  constructor() { }

  ngOnInit() {
    setInterval(() => {
      this.time = moment().format('hh : mm : ss A')
    }, 1000);

    this.day = moment().format('dddd')
  }

}

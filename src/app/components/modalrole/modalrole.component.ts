import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-modalrole',
  templateUrl: './modalrole.component.html',
  styleUrls: ['./modalrole.component.scss'],
})
export class ModalroleComponent implements OnInit {

  constructor(
    private modalCtrl: ModalController
  ) { }

  ngOnInit() {}

  _dismiss(){
    console.log("dismiss");
    this.modalCtrl.dismiss();
    //this.modalCtrl.dismiss({
      //"fromModal": "Subscribed Channel"
    //})
  }


}

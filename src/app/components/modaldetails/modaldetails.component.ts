import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { HttpClient } from '@angular/common/http';
import { LocationAccuracy } from '@ionic-native/location-accuracy/ngx';
import { HTTP } from '@ionic-native/http/ngx';
import { EnvService } from 'src/app/services/env.service';

@Component({
  selector: 'app-modaldetails',
  templateUrl: './modaldetails.component.html',
  styleUrls: ['./modaldetails.component.scss'],
})
export class ModaldetailsComponent implements OnInit {

  locationCoords: any;
  map: any;
  dateTime: Date;

  constructor(
    private modalCtrl: ModalController,
    public http: HttpClient,
    private geolocation: Geolocation,
    private locationAccuracy:LocationAccuracy,
    public https: HTTP,
    public env: EnvService,
  ){
    this.locationCoords = {
      latitude: "",
      longitude: "",
      location: "",
      accuracy: "",
      address: ""
    }
  }

  ngOnInit() {

    this.dateTime = new Date();
    this. getLocationCoordinates();
    
  }

  _dismiss(){
    console.log("dismiss");
    this.modalCtrl.dismiss();
    //this.modalCtrl.dismiss({
      //"fromModal": "Subscribed Channel"
    //})
  }

  requestGPSPermission() {
    this.locationAccuracy.canRequest().then((canRequest: boolean) => {
      if (canRequest) {
        console.log('Request successful'),
            error => {
              //Show alert if user click on 'No Thanks'
              alert('requestPermission Error requesting location permissions ' + error)
            }
      }
    });
    
  }

  askToTurnOnGPS() {
    // this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
    //   () => {
    //     // When GPS Turned ON call method to get Accurate location coordinates
    //     this.getLocationCoordinates()
    //   },
    //   error => alert('Error requesting location permissions.Please enable location setting for apps to working properly')
    // );

  }

  // Methos to get device accurate coordinates using device GPS
  getLocationCoordinates() {
    this.geolocation.getCurrentPosition().then((resp) => {
      this.locationCoords.latitude = resp.coords.latitude;
      this.locationCoords.longitude = resp.coords.longitude;

      this.locationCoords.location=this.locationCoords.latitude + ',' + this.locationCoords.longitude;

      let jsonAddress = 'https://reverse.geocoder.ls.hereapi.com/6.2/reversegeocode.json?prox=' + this.locationCoords.location + '&mode=retrieveAddresses&maxresults=1&gen=9&apiKey=' + this.env.hereApiKey;

      new Promise((resolve, reject) => {
        this.http.get(jsonAddress)
          .subscribe(response => {
            this.locationCoords.address = response['Response']['View'][0]['Result'][0]['Location']['Address']['Label'];
          }, error => { reject(error) });
      });

      // this.http.get(jsonAddress, '', '')
      //     .then(response => {
      //       let record = JSON.parse(response.data)
      //       this.address = record.Response.View[0].Result[0].Location.Address.Label;
      //     }, error => { reject(error) });
      // });

      this.locationCoords.accuracy = resp.coords.accuracy;

      this.map = 'https://image.maps.ls.hereapi.com/mia/1.6/mapview?apiKey=' + this.env.hereApiKey + '&c=' + this.locationCoords.location + '&ctr=' + this.locationCoords.location + '&z=18&h=400&w=600&style=mini&ml=eng&t=2&q=30';
      
    }).catch((error) => {
      alert('Error getting location' + error);
    });
  }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { ClockAnalogComponent } from './clock-analog/clock-analog.component';
import { HereMapComponent } from './here-map/here-map.component';
import { ModaldetailsComponent } from './modaldetails/modaldetails.component';
import { ModalComponent } from './modal/modal.component';
import { ModalroleComponent } from './modalrole/modalrole.component';
import { ModalsubscriptionComponent } from './modalsubscription/modalsubscription.component';
import { DigitalClockComponent } from './digital-clock/digital-clock.component';

@NgModule({
  declarations: [
    ClockAnalogComponent,
    HereMapComponent,
    ModaldetailsComponent,
    ModalComponent,
    ModalroleComponent,
    ModalsubscriptionComponent,
    DigitalClockComponent
  ],
  exports: [
    ClockAnalogComponent,
    HereMapComponent,
    ModaldetailsComponent,
    ModalComponent,
    ModalroleComponent,
    ModalsubscriptionComponent,
    DigitalClockComponent
  ],
  imports: [
    CommonModule,
    IonicModule
  ],
  entryComponents: [
    //PopoverComponent
  ]
})
export class ComponentModule { }
